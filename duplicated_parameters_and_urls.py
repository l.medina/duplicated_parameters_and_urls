import json
import requests
from hermetrics.osa import Osa
from tabulate import tabulate
import csv
import os
import fnmatch

URL="https://api.sjc1.defense.net/api/wipes/v2/policies/"
osa = Osa()
urls_list = []
urls_config_list = []
urls_list2 = []
param_list2 = []
param_list3 = []
param_listx = []
lst_conf = []
wildcard_urls = []
wildcard_params = []
dict1={}
dict2={}
Blank = " "

param_list = []
param_config_list = []

########################## URLS ############################
def compare_dictionaries(dict1, dict2, lst_similarity):
    NA="Not configured"
    keys = dict1.keys() | dict2.keys()
    for key in keys:
        lst_rec=[]
        try:
            if dict1[key] != dict2[key] or dict2[key] != dict1[key]:
                lst_rec.clear()
                val_url1=dict1[key]
                val_url2=dict2[key]
                lst_rec.append(key)
                lst_rec.append(val_url1)
                lst_rec.append(val_url2)
                lst_similarity.append(lst_rec)

                with open(f"Policy_{UID}.csv", 'a', newline='') as file_csv:
                    writer_csv = csv.writer(file_csv)
                    writer_csv.writerow(lst_rec)

        except KeyError:
            if key not in dict1:
                lst_rec.clear()
                val_url1 = NA
                val_url2 = dict2[key]
                lst_rec.append(key)
                lst_rec.append(val_url1)
                lst_rec.append(val_url2)
                lst_similarity.append(lst_rec)
            if key not in dict2:
                lst_rec.clear()
                val_url2 = NA
                val_url1 = dict1[key]
                lst_rec.append(key)
                lst_rec.append(val_url1)
                lst_rec.append(val_url2)
                lst_similarity.append(lst_rec)
    return lst_similarity

def get_config_URL1(urls_config_list, url1):
    dict1={}
    for x in urls_config_list:
        if url1 == x['@attr:name']:
            dict1 = x
    return dict1

def get_config_param_url1(value, key):
    dict1 = {}
    try:
        with open(f"Policy_{UID}.json", "r") as archivo_json:
            datos_json = json.load(archivo_json)
            config = datos_json["policy"]
            urls_config = config["urls"]
            url = urls_config["url"]
        for x in url:
            if value == x['@attr:name']:
                try:
                    if x["parameter"]:
                        parameter_config = x["parameter"]
                        if isinstance(parameter_config, list):
                            for z in parameter_config:
                                if key == z['@attr:name']:
                                    dict1 = z
                        else:
                            parameter_config = x["parameter"]
                            key == parameter_config['@attr:name']
                            dict1 = parameter_config
                except NameError as e:
                    print(e)
        return dict1, key
    except NameError as e:
        print(e)



def get_config_param_url2(value2, key2):
    dict2 = {}
    try:
        with open(f"Policy_{UID}.json", "r") as archivo_json:
            datos_json = json.load(archivo_json)
            config = datos_json["policy"]
            urls_config = config["urls"]
            url = urls_config["url"]
        for x in url:
            if value2 == x['@attr:name']:
                try:
                    if x["parameter"]:
                        parameter_config = x["parameter"]
                        if isinstance(parameter_config, list):
                            for z in parameter_config:
                                if key2 == z['@attr:name']:
                                    dict2 = z
                        else:
                            parameter_config = x["parameter"]
                            dict2 = parameter_config
                            key2 = x['@attr:name']

                except NameError as e:
                    print(e)
        return dict2, key2
    except NameError as e:
        print(e)




def get_config_URL2(urls_config_list, url2):
    dict2={}
    for x in urls_config_list:
        if url2 == x['@attr:name']:
            dict2 = x
    return dict2


def find_duplicated_uris():
    with open(f"Policy_{UID}.json", "r") as archivo_json:
        datos_json = json.load(archivo_json)
        config = datos_json["policy"]
        urls_config = config["urls"]
        url = urls_config["url"]
    for i in url:
        urls_list.append(i['@attr:name'])
        urls_config_list.append(i)

def find_similarity(urls_list):
    ls = [" ", " "]
    try:
        for i in urls_list:
            for x in range(len(urls_list)):
                if urls_list.index(i) != x:
                    similarity = osa.similarity(i, urls_list[x])
                    if i.startswith(urls_list[x]) and similarity > 0.80:
                        similarity=similarity*100
                        url1=i
                        url2=urls_list[x]
                        lst_similarity = []
                        dict1 = get_config_URL1(urls_config_list, url1)
                        dict2 = get_config_URL2(urls_config_list, url2)
                        compare_dictionaries(dict1, dict2, lst_similarity)
                        print(f"Similarity =========================> {similarity}")
                        print(tabulate(lst_similarity, headers=[f"{Blank}", f"URL1: {url1}", f"URL2: {url2}"], tablefmt='fancy_grid'))
                        print("\n" * 4)
    except NameError as e:
        print(e)
########################## URLS ############################

########################## PARAMETERS ######################
def find_duplicated_parameters():
    with open(f"Policy_{UID}.json", "r") as archivo_json:
        datos_json = json.load(archivo_json)
        config = datos_json["policy"]
        param_config = config["parameters"]
        param = param_config["parameter"]
    for i in param:
        param_list.append(i['@attr:name'])
        param_config_list.append(i)



def find_duplicated_parametersx():
    with open(f"Policy_{UID}.json", "r") as archivo_json:
        datos_json = json.load(archivo_json)
        config = datos_json["policy"]
        param_config = config["parameters"]
        param = param_config["parameter"]
    for i in param:
        param_listx.append(i['@attr:name'])
        param_config_list.append(i)


def find_similarity3(param_list3):
    try:
        for i in param_list3:
            for x in range(len(param_list3)):
                if param_list3.index(i) != x:
                    for key in i:
                        value = i[key]
                    for key2 in param_list3[x]:
                        value2 = param_list3[x][key2]
                    similarity = osa.similarity(value, value2)
                    similarity2 = osa.similarity(key, key2)
                    if value.startswith(value2) and similarity > 0.90:
                        if key.startswith(key2) and similarity2 > 0.80:
                            similarity = similarity * 100
                            similarity2 = similarity2 * 100
                            dict1, key = get_config_param_url1(value, key)
                            dict2, key2 = get_config_param_url2(value2, key2)
                            lst_similarity = []
                            compare_dictionaries(dict1, dict2, lst_similarity)
                            print(f"Similarity with URLs =========================> {similarity}")
                            print(f"Similarity with Parameters =========================> {similarity2}")
                            print(tabulate(lst_similarity, headers=[f" {Blank}", f"PARAMETER1: {key} --Attached to-- {value}", f"PARAMETER2: {key2} --Attached to-- {value2}"], tablefmt='fancy_grid'))
                            print("\n" * 4)
    except NameError as e:
        print(e)


def find_similarity2(param_listx):
    try:
        for i in param_listx:
            for x in range(len(param_listx)):
                if param_listx.index(i) != x:
                    similarity = osa.similarity(i, param_listx[x])
                    if i.startswith(param_listx[x]) and similarity > 0.80:
                        similarity=similarity*100
                        param1=i
                        param2=param_listx[x]
                        lst_similarity = []
                        dict1 = get_config_PARAM1(param_config_list, param1)
                        dict2 = get_config_PARAM2(param_config_list, param2)
                        compare_dictionaries(dict1, dict2, lst_similarity)
                        print(f"Similarity =========================> {similarity}")
                        print(tabulate(lst_similarity, headers=[f" {Blank}", f"PARAMETER1: {param1}", f"PARAMETER2: {param2}"], tablefmt='fancy_grid'))
                        print("\n" * 4)
    except NameError as e:
        print(e)

def get_config_PARAM1(param_config_list, param1):
    dict1={}
    for x in param_config_list:
        if param1 == x['@attr:name']:
            dict1 = x
    return dict1

def get_config_PARAM2(param_config_list, param2):
    dict2={}
    for x in param_config_list:
        if param2 == x['@attr:name']:
            dict2 = x
    return dict2


def find_duplicated_uris2():
    try:
        with open(f"Policy_{UID}.json", "r") as archivo_json:
            datos_json = json.load(archivo_json)
            config = datos_json["policy"]
            urls_config = config["urls"]
            url = urls_config["url"]
        for i in url:
            urls_list2.append(i['@attr:name'])
            try:
                if i["parameter"]:
                    param = i["parameter"]
                    if isinstance(param, list):
                        for x in param:
                            param_d = {}
                            param_name = x['@attr:name']
                            url_name = i['@attr:name']
                            param_d[param_name]=url_name
                            param_list2.append(f"{param_name}   Attached to ---->  {url_name}")
                            param_list.append(param_d)
                            param_list3.append(param_d)


                    if isinstance(param, dict):
                        param_d = {}
                        param_name = param['@attr:name']
                        url_name = i['@attr:name']
                        param_d[param_name] = url_name
                        param_list2.append(f"{param_name}   Attached to ---->  {url_name}")
                        param_list.append(param_d)
                        param_list3.append(param_d)

            except KeyError:
                pass
    except KeyError:
        print("Problem trying to get the policy configuration")
        print("Try checking-out and checking-in the WAF policy")
        print(datos_json)


def find_duplicated_parameters2():
    with open(f"Policy_{UID}.json", "r") as archivo_json:
        datos_json = json.load(archivo_json)
        config = datos_json["policy"]
        param_config = config["parameters"]
        param = param_config["parameter"]
    for i in param:
        param_list2.append(i['@attr:name'])###########
        param_list.append(i['@attr:name'])



if __name__ == "__main__":
    UID = input("Please introduce the policy UID >> ")
    wipes = "https://api.sjc1.defense.net/api/wipes/v2/policies/" + UID
    response = requests.get(wipes, verify=False, timeout=10)
    data = response.json()
    with open(f"Policy_{UID}.json", "w") as file:
        json.dump(data, file, indent=4)

    size = os.stat(f"Policy_{UID}.json").st_size
    size = size // 1024
    size = size // 1024

    find_duplicated_uris2()
    find_duplicated_parameters2()
    print("\n" * 3)


    print("Stadistics")
    print(f"Policy size...................... {size} Mb")
    print("URLs identifyed..................", len(urls_list2))
    print("Parameters identifyed............", len(param_list2))
    print("\n" * 2)



    print("[-]URLs")
    for x in urls_list2:
        print(f" ├── [+] {x}")
        for l in x:
            if l == "*":
                print(" │    └──────── [+]Wildcard URL")
                wildcard_urls.append(x)

    print("\n" * 3)
    print("[-]PARAMETERS")
    for x in param_list:
        if isinstance(x, dict):
            for key in x:
                print(f" ├── [+] {key}")
                print(f" │    └──────── [+]Attached to ---->  {x[key]}")
            for l in key:
                if l == "*":
                    print(" │    └──────── [+]Wildcard Parameter")

                    wildcard = f"{key}   Attached to ---->  {x[key]}"
                    wildcard_params.append(wildcard)

        else:
            print(f" ├── [+] {x}")
            for l in x:
                if l == "*":
                    print(" │    └──────── [+]Wildcard Parameter")
                    wildcard_params.append(x)
    print("\n" * 4)


    for w in wildcard_urls:
                if w != "*":
                    print("+===================================================================================================+")
                    print("│                         This wildcard URL contains the following URLs                             │")
                    print("+===================================================================================================+")
                    filtered = fnmatch.filter(urls_list2, w)
                    value = "│ [*] {:<94}│".format(w)
                    print(value)
                    for x in filtered:
                        if w != x:
                            value2 = "│  └─────────>    {:<82}│".format(x)
                            print(value2)
                    print("+___________________________________________________________________________________________________+")
                    print("\n" * 3)




    for w in wildcard_params:
        if w != "*":
            print("+===================================================================================================+")
            print("│                    This wildcard PARAMETER contains the following PARAMETERs                      │")
            print("+===================================================================================================+")
            filtered = fnmatch.filter(param_list2, w)
            value = "│ [*] {:<94}│".format(w)
            print(value)
            for x in filtered:
                if w != x:
                    value2 = "│  └─────────>    {:<82}│".format(x)
                    print(value2)
            print("+___________________________________________________________________________________________________+")
            print("\n")




    while True:
        print("""
        1.- Find duplicated URLs
        2.- Find duplicated Parameters
        3.- Exit
        """)


        option = input("OPTION >> ")
        if option == "1":
            find_duplicated_uris()
            find_similarity(urls_list)
            urls_list.clear()

        if option == "2":
            find_similarity3(param_list3)
            find_duplicated_parametersx()
            find_similarity2(param_listx)
            param_list3.clear()
            param_listx.clear()

        if option == "3":
            break

        else:
            print("PLEASE CHOOSE A VALID OPTION ")